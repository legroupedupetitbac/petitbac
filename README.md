# Le petit bac

## But du jeu

Le jeu implémenté est le petit bac, jouable en réseau par une multitude joueurs.

## Fonctionnement initial

- Lancement de main.sh par le serveur
- Réception des joueurs qui s'inscrivent sur le serveur avec leurs adresses
- Génération de la partie pour tous les joueurs : génération de la lettre
- Génération des catégories pour tous les joueurs
- Remplissage par les joueurs. Des que le premier décide de terminer son remplissage, le jeu s'arrête.
- Réception des fichiers réponses de tous les clients
- Comptage des points et détermination du gagnant
- Envoi du gagnant et des scores aux joueurs

## Fonctionnement réel

Le jeu n'est malheureusement pas terminé.
Au stade où l'on est :

    D'un point de vue réseau :
- Les clients peuvent se connecter sur le serveur
- Les clients peuvent s'envoyer des messages, déchiffrer les messages des autres et exécuter les fonctions des messages
    D'un point de vue jeu:
- Jouer en solo avec affichage des catégories
- Les bases de données sont créées et sont fonctionnelles mais malheureusement la fonction pour analyser les réponses demeure inachevée.

