#!/usr/bin/env bash

echo "PROJET API : LE PETIT BAC."

./configurer.sh

PRET="False"
while [ "$PRET" = "False" ]
do
    source reception_msg_prep_jeu.sh
    sleep 2
done

./generation_partie.sh

FINI="False"
while [ "$FINI" = "False" ]
do
    source reception_rep_premier_joueur.sh
    sleep 2
done

./fin_manche_pour_tous.sh
./verification_reponses.sh
./determination_gagnant.sh
./envoie_gagnant.sh
./fin_de_partie.sh
