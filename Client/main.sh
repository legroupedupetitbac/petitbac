#!/usr/bin/env bash

echo "PROJET API : LE PETIT BAC."

./configurer.sh

PRET="False"
while [ "$PRET" = "False" ]
do
    source reception_msg_prep_jeu.sh
done

./generation_partie.sh

FINI=0
while [ "$FINI" = 0 ]
do
    source interface_jeu.sh
    echo "$FINI"
    # source reception_rep_premier_joueur.sh
done

./fin_manche_pour_tous.sh
./verification_reponses.sh
./determination_gagnant.sh
./envoie_gagnant.sh
./fin_de_partie.sh
