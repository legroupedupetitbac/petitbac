#!/usr/bin/env bash


categories="nom pays animal metier objet vegetal"


function choix_categories {
    echo "Veuillez choisir une catégorie :"
    index=1
    for elt in $categories
    do
        echo "$index - $elt"
        ((index++))
    done
    echo "7 - terminer la partie"
    reponse=0
    while ! [[ $reponse =~ ^[1-7]$ ]]; do
        read -r reponse
        if ! [[ $reponse =~ ^[1-7]$ ]]; then
            echo "Veuillez entrer un chiffre entre 1 et 7."
        fi
    done

    return  "$reponse"
}



function maj_categories {
	debut=$(pwd)
	cd categories/
	choix_categories
	rep=$?
	if [ $rep = 1 ]
	then
		read -p "entrez un nom: " nom
		echo $nom >  nom.txt

	elif [ $rep = 2 ]
        then
		read -p "entrez un pays: " pays
		echo $pays >  pays.txt

	elif [ $rep = 3 ]
	then
                read -p "entrez un animal: " animal
		echo $animal >  animal.txt

	elif [ $rep = 4 ]
	then
                read -p "entrez un metier: " metier
		echo $metier >  metier.txt

	elif [ $rep = 5 ]
	then
                read -p "entrez un objet: " objet
		echo $objet >  objet.txt

	elif [ $rep = 6 ]
	then
                read -p "entrez un vegetal: " vegetal
		echo $vegetal >  vegetal.txt


	elif [ $rep = 7 ]
	then
		terminaison
		export FINI=$?
	fi
	cd $debut
}



function terminaison {
    for k in $categories
    do
        f="${k}.txt"
        if [ ! -s "$f" ]; then
            echo "le fichier $k est vide"
            fin_valide=0
	    return $fin_valide
        else
            fin_valide=1
        fi
    done
    return $fin_valide
}

function reinitialiser {
    cd categories/
    for k in $categories
    do
        f="${k}.txt"
	echo "$f"
	rm -f "$f"
    done
}


maj_categories


